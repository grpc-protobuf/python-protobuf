import simple.complex_pb2 as complex_pb2

complex_message = complex_pb2.ComplexMessage()

# default_message = complex_pb2.DummyMessage()
# default_message.id = 1
# default_message.content = "Test Content"


complex_message.default_message.id = 123
complex_message.default_message.content = "Hello Content"

first_message = complex_message.other_messages.add()
first_message.id = 890
first_message.content = "First list message"

second_message = complex_message.other_messages.add()
second_message.id = 890
second_message.content = "Second list message"

complex_message.other_messages.add(
    id=678,
    content="new type content"
)
print(complex_message)
