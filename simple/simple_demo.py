import simple.simple_pb2 as simple_pb2

simple_message = simple_pb2.SimpleMessage()
simple_message.gender = simple_pb2.Gender.MALE
simple_message.content = "Hello!"
names = simple_message.names
names.append("Ashish")
names.append("John")

print(simple_message)

# wb: write binary
with open("simple.bin", "wb") as f:
    print("Writing content")
    bytesAsString = simple_message.SerializeToString();
    f.write(bytesAsString)

# rb: read binary

with open("simple.bin", "rb") as f:
    print("Reading contents")
    simple_message_read = simple_pb2.SimpleMessage().FromString(f.read())
    print(simple_message_read)
    print("Gender: " + str(simple_message_read.gender == simple_pb2.Gender.FEMALE))
